# SSL-Certification-As-Service

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![CI Status](https://img.shields.io/badge/CI-success-brightgreen)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Group](https://img.shields.io/badge/Group-CBZ%20D--velop-blue)
![Group](https://img.shields.io/badge/Group-Adeptus%20Automata%20Silici-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Cbz-blue)

## Description

![Tag: GitLab-CI](https://img.shields.io/badge/Tech-GitLab--Ci-orange)
![Tag: DevOps](https://img.shields.io/badge/Tech-DevOps-orange)
![Tag: DevSecOps](https://img.shields.io/badge/Tech-DevSecOps-orange)
![Tag: GitOps](https://img.shields.io/badge/Tech-GitOps-orange)
![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: TLS](https://img.shields.io/badge/Tech-TLS-orange)
![Tag: mTLS](https://img.shields.io/badge/Tech-mTLS-orange)

## Introduction

This project focuses on an automated certificate generation service powered by Ansible playbooks.

In today's interconnected world, ensuring data security is paramount. Online communications demand robust protection against eavesdropping and breaches of confidentiality. SSL/TLS (Secure Sockets Layer / Transport Layer Security) protocols and their derivatives, such as mTLS (mutual TLS), play a pivotal role in upholding security standards by providing authentication, confidentiality, and data integrity.

The primary objective is to streamline the creation of SSL/TLS certificates based on a structured architecture defined within a YAML file.

## SSL/TLS/mTLS Context and Challenges

SSL/TLS serves as a fundamental protocol for securing online communications by encrypting data and ensuring server authenticity. Digital certificates play a vital role in authenticating parties communicating online. The deployment of digital certificates is crucial in modern information security, particularly in financial transactions, government communications, and everyday online interactions.

TLS certification presents several challenges:

* Confidentiality: Protecting sensitive data by encrypting it to prevent malicious interceptions.
* Authentication: Verifying the identity of parties communicating online to prevent "Man-in-the-Middle" attacks.
* Integrity: Ensuring data exchanged between parties remains unaltered during transit.
* Server Security: Shielding servers from attacks by validating client identities.
* Access Control: Restricting access to online resources to authorized users only.

## The SSL Certification Service

This tool, leveraging Ansible, offers a comprehensive solution for creating and managing TLS certification architectures, effectively addressing the aforementioned security challenges. Operating from a YAML input file and an Ansible playbook, our tool encompasses the following key features:

* Root Authority Creation: Capability to create Root Authorities, establishing the highest level of trust.
* Intermediate Certificates: Generation of intermediate certificates with the ability to validate others.
* Cascade Signing: Signing multiple intermediate certificates with a single Root Authority.
* Output Products: Generation of essential output products for certificate management, including certificates in PEM format, keys, Certificate Signing Request (CSR), bundles, and CA chains.

* **Note: If the same certificate is requested multiple times, the service will revoke the previous one and create a new one upon request. Previous CAs are retained between launches, but not CERTs.**

## Getting Started

### Prerequisites

To use this tool, you'll need:

* Docker installed on your system.
* A YAML input file with the desired certification architecture configuration (default is `pkies/CA_NAME/input.yml`).

### Usage

* Clone this repository to your local machine.
* Navigate to the project directory.
* Go into the `./bin` folder and run the `./decrypt` script.
* Edit the `.gpg.key` file to generate a key for certificate encryption.
* Edit the `.ansible-vault.key` file to generate a key for Ansible input encryption.
* Edit the `.remote-storage.env` file and fill in your Nexus/Artifactory information.
* Initially generate your CA (only if you prefer not to redeploy all your CA each time).
* This project includes a pre-commit script to encrypt your content (Ansible input files, CAs, private keys) using GPG. Scripts in `./bin` allow for decryption/encryption at any time. You can locally push changes to your remotes using the `./bin/push-ca-to-remote` script.
* **Remove** the `.ansible-vault`, `.gpg.key` and `.remote-storage.env` from the `.gitignore`.

The Ansible playbook relies on all vars in `default/examples`. Ensure a non-empty list of CA and CERTs for correct playbook execution. Store CA key passwords inside the input folder, hence everything is encrypted.

Organizing CAs and sub-CAs in separate folders, as currently structured, improves readability and prevents lengthy YAML files.

To expedite certification as a service, add CICD `REMOTE_STORAGE_ENV_BASE64` var in your pipeline, that contains the `.remote-storage.env` file in BASE64 as follows:

```shell
REMOTE_REPOSITORY_URL="https://my.remote.storage.domain.tld"
REMOTE_REPOSITORY_NAME="my-repository"
REMOTE_REPOSITORY_ADDRESS="${REMOTE_REPOSITORY_URL}/repository/${REMOTE_REPOSITORY_NAME}"
REMOTE_CREDENTIALS_USER="user"
REMOTE_CREDENTIALS_PASSWORD="secret"
```

Add an `ANSIBLE_VAULT_KEY` var, that contains the `.ansible-vault.key` as follows:

```shell
# .ansible-vault.key
YOUR-LONG-AND-SECRET-KEY-HERE
```

And finally a `GPG_KEY` var, that contains the `.gpg.key` key value as follows:

```shell
GPG_PASS=YOUR-LONG-AND-SECRET-PASS-HERE
```

**Note:** You do not need to encode the file, just create a string.

You will probably need to setup `SSH` environement; to do that, you can define `SSH_PRIVATE_KEY_BASE64` and `SSH_PUBLIC_KEY_BASE64` in your CICD vars.

Masking vars in this manner facilitates their modification. More information inside the GitLab CI file.

### Basic commands

```shell
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline
```

### Linters

```shell
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .
```

### Push Certificates to Remote Storage

The `push-ca-to-remote` script uploads all certificates, private keys, and bundles into a remote storage based on the `.remote-storage.env` file. The push is done using the `curl` command, so the remote storage must support basic REST operations. Basic authentication is handled by the script.

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-07-26: First Init

* First init of this playbook with the `bootstrap_playbook` playbook by Lord Robin Crombez
* Added `bin/certification` script to build custom SSL certification layers

### 2023-12-08: CI and SSL as service

* This project retains or creates all your data/certs.
* Allows creation and push of certs to a Nexus/Artifactory repository.
* Handles revocation and rewriting.
* CICD Push certs.

### 2023-12-19: Cert push fix and pre-commit script

* Added pre-commit script in `./bin` (don't forget to install in your `.git/hooks` and `chmod +x`)
* Fixed the push script: certs weren't pushed

### 2023-12-19

* Changed vars and change CICD for better performances
* Added SonarQube for quality checks
* CICD contains now a "publish" stage, GitLab Release will be created on "main", and certificates will be published on "main" too, based on timestamp
* You can edit the CICD to change the publish output and set "latest" version for crypto material
* CICD push to a private remote Nexus, so `${VERSION-TIMESTAMP}` is used for identification

### 2024-06-17: Reworks and automation

* Repos now use custom generation to create certification pipeline
* Added detect secret to the CI
* Added Markdown/YAML/Ansible/secret steps to the pipeline
* Fixed and edited vars to follow new guidelines
* Fix bundles and ZIP content to the crypt process

### 2024-11-17: Refactoring and hooks

* Use latest version of tools
* Add real git hook system

### 2024-12-27: Global refactoring and upgrades

* Usage of Docker for run when needed
* Upgrade CICD
* Use latest version of Ansible playbook
* Use BASE64 files in CICD for remote-storage options
* Rewrite README and add YAML validation

## Authors

* Lord Robin Crombez

## Sources

* [Ansible playbook documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_playbooks.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
