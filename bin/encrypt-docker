#!/bin/bash
cd ./../

dos2unix ./* || true
dos2unix ./.* || true

source "./.gpg.key" || exit
chmod -x ./.ansible-vault.key

echo "====================== ENCRYPT FILES: INPUTS ======================"
# Use Ansible vault function to encrypt all input files
for CA_FOLDER in ./pkies/*; do
    CA_FOLDER="${CA_FOLDER}"
    if [ -d "${CA_FOLDER}" ]; then
        echo "New CA to encrypt detected: $CA_FOLDER"
        cd "${CA_FOLDER}"
        DIR=$(basename "$(pwd)")

        # Use Ansible vault function to encrypt all input files
        echo "Encrypting input files with the Ansible Vault in $CA_FOLDER"
        MSYS_NO_PATHCONV=1 docker run --rm \
            -v "$(pwd)/../../:/app" \
            -w "/app" \
            labocbz/build-pkies:latest \
            /bin/sh -c "\
            cat ./.ansible-vault.key > /tmp/.ansible-vault.key && \
            chmod -x /tmp/.ansible-vault.key && \
            ansible-vault encrypt \"${CA_FOLDER}/input.yml\" --vault-password-file /tmp/.ansible-vault.key"
        cd ./../../
    fi
done
echo "====================== ENCRYPT FILES: INPUTS DONE ======================"

echo "====================== ENCRYPT FILES: PRIVATE KEYS ======================"
# Use GPG to encrypt all private key files
for CA_FOLDER in ./outputs/ssl/*; do
    echo "Encrypting private key files in $CA_FOLDER"
    for PRIVATE_KEY in ${CA_FOLDER}/private/*; do
        echo "Checking for private key: $PRIVATE_KEY"
        if [ -f "${PRIVATE_KEY}" ]; then
            if [ "${PRIVATE_KEY: -4}" == ".gpg" ] || [[ "${PRIVATE_KEY}" == *"lock"* ]]; then
                echo "Skipping encrypted private key: $PRIVATE_KEY"
            else
                echo "Encrypting private key with with GPG: $PRIVATE_KEY"
                gpg --batch --passphrase "${GPG_PASS}" -c --symmetric "${PRIVATE_KEY}" && rm "${PRIVATE_KEY}"
            fi
        fi
    done
done
echo "====================== ENCRYPT FILES: PRIVATE KEYS DONE ======================"
echo "====================== ENCRYPT FILES: BUNDLES ======================"
# Use GPG to encrypt all private bundles files
for CA_FOLDER in ./outputs/ssl/*; do
    echo "Encrypting bundle files in $CA_FOLDER"
    for BUNDLE in ${CA_FOLDER}/bundles/*.zip; do
        echo "Checking for bundle: $BUNDLE"
        if [ -f "${BUNDLE}" ]; then
            if [ "${BUNDLE: -4}" == ".gpg" ] || [[ "${BUNDLE}" == *"lock"* ]]; then
                echo "Skipping encrypted bundle: $BUNDLE"
            else
                echo "Encrypting bundle with with GPG: $BUNDLE"
                gpg --batch --passphrase "${GPG_PASS}" -c --symmetric "${BUNDLE}" && rm "${BUNDLE}"
            fi
        fi
    done

    # Encrypt key files in bundle folders
    for BUNDLE in ${CA_FOLDER}/bundles/*; do
        echo "Checking for key file to encrypt in bundle: $BUNDLE"
        if [ -d "${BUNDLE}" ]; then
            for PRIVATE_KEY in ${BUNDLE}/*.key; do
                if [ -f "${PRIVATE_KEY}" ]; then
                    gpg --batch --passphrase "${GPG_PASS}" -c --symmetric "${PRIVATE_KEY}" && rm "${PRIVATE_KEY}"
                fi
            done
        fi
    done
done
echo "====================== ENCRYPT FILES: PRIVATE BUNDLES DONE ======================"
