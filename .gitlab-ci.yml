---

# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

variables:
  #
  DOCKER_REGISTRY: "docker-registry.nexus3.labo-cbz.net"
  DOCKER_IMAGE__DEVOPS_SONAR_SCANNER: "${DOCKER_REGISTRY}/labocbz/devops-sonar-scanner:latest"
  DOCKER_IMAGE__DEVOPS_LINTERS: "${DOCKER_REGISTRY}/labocbz/devops-linters:latest"
  DOCKER_IMAGE__BUILD_PKIES: "${DOCKER_REGISTRY}/labocbz/build-pkies:latest"
  DOCKER_IMAGE__ALPINE: "${DOCKER_REGISTRY}/alpine:latest"

stages:
  - "gitlab-test"
  - "lint"
  - "sonarqube"
  - "load-dynamic-steps"
  - "build-and-publish"

###############################################################################
#################### EXTENDABLES
###############################################################################

###############################################################################
#################### CI JOBS
###############################################################################

############################################
#### gitlab-test
############################################

# As a GitLab ci, we use the default template :)
sast:
  stage: "gitlab-test"
include:
  - template: "Security/SAST.gitlab-ci.yml"

############################################
#### lint
############################################

lint:markdowns:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "markdownlint './README.md' --disable MD013"
    - "markdownlint './*/*/README.md' --disable MD013"
  allow_failure: false

lint:secrets:
  stage: "lint"
  image: "${DOCKER_IMAGE__DEVOPS_LINTERS}"
  script:
    - "detect-secrets scan --exclude-files 'node_modules' --baseline .secrets.baseline"
    - "detect-secrets audit .secrets.baseline"
  allow_failure: false

############################################
#### sonarqube
############################################

sonarqube:
  stage: "sonarqube"
  image:
    name: "${DOCKER_IMAGE__DEVOPS_SONAR_SCANNER}"
    entrypoint: [""]
  needs:
    - "lint:markdowns"
    - "lint:secrets"
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - ".sonar/cache"
  script:
    - "sonar-scanner"

############################################
#### load-dynamic-steps
############################################

load-dynamic-steps:
  stage: "load-dynamic-steps"
  needs:
    - "sonarqube"
  trigger:
    include:
      - local: ".gitlab-ci.dynamic-steps.yml"
    strategy: "depend"
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_COMMIT_BRANCH == "main"'
